package kr.co.softbridge.sobrochatserver.sample.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import kr.co.softbridge.sobrochatserver.commons.dto.TokenReqDto;
import kr.co.softbridge.sobrochatserver.commons.dto.SampleDto;
import kr.co.softbridge.sobrochatserver.sample.mapper.SampleMapper;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class SampleService {

    private static final Logger logger = LogManager.getLogger(SampleService.class);

    private final SampleMapper sampleMapper;

	public SampleDto authenticate(TokenReqDto param) {
		return sampleMapper.getSampleInfo(param);
	}

}
