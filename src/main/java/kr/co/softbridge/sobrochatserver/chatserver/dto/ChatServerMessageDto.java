package kr.co.softbridge.sobrochatserver.chatserver.dto;

import groovy.transform.builder.Builder;
//import lombok.Builder;
import lombok.*;

/***
/**
 * 채팅 서버 Message DTO
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ChatServerMessageDto {

//    private ChatServerMessageTypeDto type;
    private String type;
    private String content;
    private String sender;

    private String roomNo;
//    public enum MessageType {
//        ENTER, TALK
//    }

//    public ChatServerMessageTypeDto getType() {
//        return type;
//    }

//    public void setType(ChatServerMessageTypeDto type) {
//        this.type = type;
//    }

//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//
//    public String getSender() {
//        return sender;
//    }
//
//    public void setSender(String sender) {
//        this.sender = sender;
//    }
}
