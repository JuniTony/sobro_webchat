package kr.co.softbridge.sobrochatserver.commons.controller;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import kr.co.softbridge.sobrochatserver.commons.dto.MonitoringRealTimeChatRoomResDto;
import kr.co.softbridge.sobrochatserver.commons.service.MonitoringLogService;
import kr.co.softbridge.sobrochatserver.commons.util.StringUtil;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/monitoringlog")
@RestController
public class MonitoringLogController {

	@Autowired
    private MonitoringLogService monitoringLogService;

	@ApiOperation(value = "실시간 채널", notes = "방송 모니터링")
	@PostMapping(value = "/realTimeLog", produces = MediaType.APPLICATION_JSON_VALUE)
	public void realTimeLog(
			HttpServletResponse requestHeader
			, @RequestBody(required = true) HashMap<String, Object> paramMap
			) {
		String pattern = "yyyy-MM-dd HH:mm:ss";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String startServerTime = simpleDateFormat.format(new Date());
    	
    	// 서비스 호출
    	monitoringLogService.chatRoomRealTimeLog(requestHeader, paramMap);

    	String endServerTime = simpleDateFormat.format(new Date());
    	
	}
	
	@ApiOperation(value = "실시간 채널", notes = "방송 모니터링")
	@PostMapping(value = "/realTimeChatRoomList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MonitoringRealTimeChatRoomResDto> realTimeChatRoomList(
			HttpServletRequest request
			, @RequestBody(required = true) HashMap<String, Object> paramMap
			) throws URISyntaxException {
		String pattern = "yyyy-MM-dd HH:mm:ss";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String startServerTime = simpleDateFormat.format(new Date());
    	
    	paramMap.put("svcCode", StringUtil.null2void(paramMap.get("svcCode"), startServerTime));
    	paramMap.put("nowTime", startServerTime);
    	
    	// 서비스 호출
    	ResponseEntity<MonitoringRealTimeChatRoomResDto> result = monitoringLogService.getChatRoomList(paramMap);

    	String endServerTime = simpleDateFormat.format(new Date());
    	monitoringLogService.apiCallLog(startServerTime, endServerTime, "실시간 방송목록", "R", result.getBody().getResultCode(), paramMap, result);

    	return result;
	}
}
