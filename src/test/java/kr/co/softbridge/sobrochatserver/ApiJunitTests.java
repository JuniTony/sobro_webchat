package kr.co.softbridge.sobrochatserver;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@AutoConfigureMockMvc
@SpringBootTest
public class ApiJunitTests {
	
	private static final Logger logger = LogManager.getLogger(ApiJunitTests.class);
	
	/* 필수 작성 코드 Start */
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	private String svcCode = "rumOWCvxRdpswCdLJI6Y";
	
	@BeforeAll
	static void beforeAll() {
	}
	
	@BeforeEach
	public void beforeEach() {

		objectMapper = Jackson2ObjectMapperBuilder.json()
				.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
				.modules(new JavaTimeModule())
				.build();
	}
	
	public void postResult(String url, String content) throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.post(url)
				.content(content)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(result -> {
					MockHttpServletResponse response = result.getResponse();
					if(response.getStatus() == 200) {
						status().isOk();
					} else if(response.getStatus() == 201) {
						status().isCreated();
					} else {
						status().isNoContent();
					}
					logger.info(response.getContentAsString());
				});
	}
	
	public void getResult(String url, String content) throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get(url)
				.content(content)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(result -> {
					MockHttpServletResponse response = result.getResponse();
					if(response.getStatus() == 200) {
						status().isOk();
					} else if(response.getStatus() == 201) {
						status().isCreated();
					} else {
						status().isNoContent();
					}
					logger.info(response.getContentAsString());
				});
	}
	/* 필수 작성 코드 End */
	
	// 채팅 서버 서비스 체크 테스트
	@Test
	public void getChatServerService() throws Exception{
		String url = "/chatserverservice";

		getResult(url, "");
	}

	// 공통코드 체크 테스트
	@Test
	public void getCodeList() throws Exception{
		String url = "/common/list";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("grpCode", "01");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 실시간 채널 Log 체크 테스트
	@Test
	public void realTimeLog() throws Exception{
		String url = "/common/realTimeLog";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLrsKnshqHsnpAxIiwicGluIjoiMTEyMzMzMTM2MDEzNDgyNTciLCJzdmNDb2RlIjoiaTR6Q2h4T2ljY3hMVmJUMnJBYUoiLCJyb29tQ29kZSI6IjI1NTY2IiwidXNlcklkIjoic3RyZWFtZXIxIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDAwNjEzLCJleHAiOjE2MzM0MjYyMDB9.07TVQ6oILhMlPX0EvaSMK3Qir1L46BS_mEsQkVW7bxqnLpC1oHk-MlX1xQNsZHufkdbcn05RS4eruAKTyIeQnw");
		paramMap.put("roomCode",		"300");
		paramMap.put("userId",			"0");
		paramMap.put("videoBitrate",	"0");
		paramMap.put("videoWidth",		"0");
		paramMap.put("videoHeight",		"0");
		paramMap.put("videoFrameRate",	"0");
		paramMap.put("videoAspectRatio","0");
		paramMap.put("audioBitrate",	"0");
		paramMap.put("audioLaency",		"0");
		paramMap.put("audioSampleRate",	"0");
		paramMap.put("audioSampleSize",	"0");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 실시간 채널 목록 체크 테스트
	@Test
	public void realTimeRoomList() throws Exception{
		String url = "/common/realTimeRoomList";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcCode", svcCode);
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 토큰 생성 체크 테스트
	@Test
	public void auth() throws Exception{
		String url = "/login/auth";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcDomain", "https://localhost:8080");
		paramMap.put("svcCode", svcCode);
		paramMap.put("svcNm", "sobro junit서비스");
		paramMap.put("userId", "streamer1");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 서비스토큰검증 테스트
	@Test
	public void roomVerify() throws Exception{
		String url = "/login/auth/roomVerify";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLrsKnshqHsnpAxIiwicGluIjoiMTEyMzMzMTM2MDEzNDgyNTciLCJzdmNDb2RlIjoiaTR6Q2h4T2ljY3hMVmJUMnJBYUoiLCJyb29tQ29kZSI6IjI1NTY2IiwidXNlcklkIjoic3RyZWFtZXIxIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDAwNjEzLCJleHAiOjE2MzM0MjYyMDB9.07TVQ6oILhMlPX0EvaSMK3Qir1L46BS_mEsQkVW7bxqnLpC1oHk-MlX1xQNsZHufkdbcn05RS4eruAKTyIeQnw");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// Room토큰검증 테스트
	@Test
	public void svcVerify() throws Exception{
		String url = "/login/auth/svcVerify";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 회의입장 체크 테스트
	@Test
	public void roomJoin() throws Exception{
		String url = "/login/roomJoin";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		paramMap.put("roomCode", "300");
		paramMap.put("roomPw", "YbN0TPn4qb/IcZ51UmBhHw==");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 회의종료 체크 테스트
	@Test
	public void roomOut() throws Exception{
		String url = "/login/roomOut";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 회의목록 체크 테스트
	@Test
	public void roomList() throws Exception{
		String url = "/room/list";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		paramMap.put("roomType", "01");
		paramMap.put("userId", "streamer1");
		paramMap.put("page", "1");
		paramMap.put("pageSize", "50");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 회의등록 체크 테스트
	@Test
	public void roomCreate() throws Exception{
		String url = "/room/create";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		paramMap.put("roomType", "01");
		paramMap.put("maxPeople", "5");
		paramMap.put("title", "JUINT TEST");
		paramMap.put("quality", "01");
		paramMap.put("viewYn", "Y");
		paramMap.put("startDt", "2021-10-12 10:00:00");
		paramMap.put("endDt", "2021-10-12 11:00:00");
		paramMap.put("roomPw", "1234");
		paramMap.put("userId", "streamer1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 방수정 체크 테스트
	@Test
	public void roomUpdate() throws Exception{
		String url = "/room/update";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		paramMap.put("roomType", "01");
		paramMap.put("maxPeople", "5");
		paramMap.put("title", "JUINT TEST");
		paramMap.put("quality", "01");
		paramMap.put("viewYn", "Y");
		paramMap.put("startDt", "2021-10-12 10:00:00");
		paramMap.put("endDt", "2021-10-12 23:00:00");
		paramMap.put("roomPw", "1234");
		paramMap.put("userId", "streamer1");
		paramMap.put("roomCode", "300");
		paramMap.put("oldRoomPw", "YbN0TPn4qb/IcZ51UmBhHw==");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 방삭제 체크 테스트
	@Test
	public void roomDelete() throws Exception{
		String url = "/room/delete";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("roomPw", "YbN0TPn4qb/IcZ51UmBhHw==");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 바로시작 체크 테스트
	@Test
	public void fastStart() throws Exception{
		String url = "/room/fastStart";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		paramMap.put("roomType", "01");
		paramMap.put("maxPeople", "5");
		paramMap.put("title", "JUINT TEST");
		paramMap.put("quality", "01");
		paramMap.put("viewYn", "Y");
		paramMap.put("startDt", "2021-10-12 10:00:00");
		paramMap.put("endDt", "2021-10-12 23:00:00");
		paramMap.put("roomPw", "1234");
		paramMap.put("userId", "streamer1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 회의재시작 체크 테스트
	@Test
	public void reStartStatus() throws Exception{
		String url = "/room/reStartStatus";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwaW4iOiIxNTM0NDAxNDUyMzc1MDg0NiIsInN2Y0NvZGUiOiJydW1PV0N2eFJkcHN3Q2RMSkk2WSIsInVzZXJJZCI6InN0cmVhbWVyMSIsInN2Y0RvbWFpbiI6Imh0dHBzOi8vbG9jYWxob3N0OjgwODAiLCJzdWIiOiJ1c2VyLWxvZ2luIiwiaWF0IjoxNjM0MDIwNDgwLCJleHAiOjE2MzQwMjQwODB9.XgDSYFZ8uzCiV9AC6VdFQbGAuBr7qFFsnn2cL62lTicxsV2ShsCSryEIHKaYl6ypg9gBMfm1PPhiMj3g8GvJzw");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 파일목록 체크 테스트
	@Test
	public void fileList() throws Exception{
		String url = "/file/list";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("page", "1");
		paramMap.put("pageSize", "50");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 파일업로드 체크 테스트
	@Test
	public void fileUpload() throws Exception{
		String url = "/file/upload";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("svcTarget", "R0");
		paramMap.put("file", "");
		paramMap.put("shareType", "01");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 파일다운로드 체크 테스트
	@Test
	public void fileDownload() throws Exception{
		String url = "/file/download";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("fileSeq", "1");
		paramMap.put("filePath", "black/i4zChxOiccxLVbT2rAaJ/25566-20211005113454088.png");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 파일삭제 체크 테스트
	@Test
	public void fileDelete() throws Exception{
		String url = "/file/delete";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("fileSeq", "1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 화이트보드 파일업로드 체크 테스트
	@Test
	public void whiteBoardUpload() throws Exception{
		String url = "/file/whiteBoardUpload";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("fileName", "ss_image.png");
		paramMap.put("fileExtension", "png");
		paramMap.put("encodedFile", "iVBORw0KGgoAAAANSUhEUgAAASoAAAEsCAYAAAB0Y/4yAAAABHNCSVQICAgIfAhkiAAAEmpJREFUeJzt3XuspVV5x/HvSyaTCUFC6YQaQ");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 회의록 저장 체크 테스트
	@Test
	public void meetingLogSave() throws Exception{
		String url = "/meetinglog/save";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("saveType", "C");
		paramMap.put("contents", "회의록을 저장합니다.");
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 회의록 보기 체크 테스트
	@Test
	public void meetingLogView() throws Exception{
		String url = "/meetinglog/view";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 퀴즈생성 체크 테스트
	@Test
	public void quizCreate() throws Exception{
		String url = "/quiz/quizCreate";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("title", "퀴즈 생성");
		paramMap.put("runningTime", "30");
		paramMap.put("quizInfoList", "");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 퀴즈수정 체크 테스트
	@Test
	public void quizUpdate() throws Exception{
		String url = "/quiz/quizUpdate";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("quizNo", "1");
		paramMap.put("title", "퀴즈 수정");
		paramMap.put("runningTime", "30");
		paramMap.put("quizInfoList", "");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 퀴즈삭제 체크 테스트
	@Test
	public void quizDelete() throws Exception{
		String url = "/quiz/quizDelete";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("quizNo", "1");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);
		
		postResult(url, content);
	}

	// 퀴즈목록 체크 테스트
	@Test
	public void quizList() throws Exception{
		String url = "/quiz/quizList";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("presenterYn", "U");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 퀴즈상세 체크 테스트
	@Test
	public void quizDetail() throws Exception{
		String url = "/quiz/quizDetail";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("quizNo", "1");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 퀴즈상태변경 체크 테스트
	@Test
	public void quizChange() throws Exception{
		String url = "/quiz/quizChange";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("quizNo", "1");
		paramMap.put("quizChange", "123");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}

	// 퀴즈답안제출 체크 테스트
	@Test
	public void answerSubmit() throws Exception{
		String url = "/quiz/answerSubmit";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		paramMap.put("userNm", "방송자1");
		paramMap.put("quizNo", "1");
		paramMap.put("quizAnswerList", "");
		
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}
	
	// 방 토큰 재사용 테스트
	@Test
	public void roomTokenReuse() throws Exception{
		String url = "/login/roomTokenReuse";
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roomToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyTm0iOiLssLjsl6zsnpAxIiwicGluIjoiMTYyMDE0NjAzMzYxODI0NzUiLCJzdmNDb2RlIjoiZDkya2c5c2Y4ZHN3YzhzYTgxMCIsInJvb21Db2RlIjoiMTY5IiwidXNlcklkIjoiUlUyMDIxMTAyNzgxNjIwMTQ1ODYzNzYwIiwic3ViIjoiYnJvYWRjYXN0aW5nLWxvZ2luIiwiaWF0IjoxNjMzNDE4NDE0LCJleHAiOjE2MzM0NDQyMDB9.55W0R8wrk33BM3jetyuBozY1AQNfiAt_RBqpEZTDuKIKJCieJQH-KnTmUNX6rq8Vzjb3XvopAmh2-kPnpw7Pzg");
		paramMap.put("roomCode", "300");
		paramMap.put("userId", "streamer1");
		
		String content = objectMapper.writeValueAsString(paramMap);
		logger.info(content);

		postResult(url, content);
	}
}
