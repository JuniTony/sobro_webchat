package kr.co.softbridge.sobrochatserver.commons.dto;

import java.sql.Date;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoomRequestDto {	
	@ApiParam(value = "조회권한", required = false, example = "A|M|U")
	private	String	searchAuth;
	
	@ApiParam(value = "방송코드", required = true, example = "1")
	private	int		chatRoomCode;
	
	@ApiParam(value = "사용자ID", required = false, example = "admin01")
	private	String	memberId;
	
	@ApiParam(value = "조회 시작일", required = false, example = "20210101")
	private	Date	searchStartDt;
	
	@ApiParam(value = "조회 종료일", required = false, example = "20210131")
	private	Date	searchEndDt;

}
