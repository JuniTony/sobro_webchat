package kr.co.softbridge.sobrochatserver.chatlogin.mapper;

import java.util.HashMap;
import java.util.Map;

import kr.co.softbridge.sobrochatserver.commons.annotation.PrimaryMapper;

import kr.co.softbridge.sobrochatserver.commons.dto.ChatRoomDto;
import kr.co.softbridge.sobrochatserver.commons.dto.SvcInfoDto;
import kr.co.softbridge.sobrochatserver.commons.dto.TokenResDto;
import kr.co.softbridge.sobrochatserver.chatlogin.dto.ChatLiveUserDto;

@PrimaryMapper
public interface ChatLoginMapper {

	ChatLiveUserDto checkChatRoomUser(Map<String, Object> paramMap);

	ChatRoomDto getChatRoom(Map<String, Object> paramMap);

	int insertChatRoomUser(Map<String, Object> paramMap);

	String getChatUserId();

	ChatLiveUserDto getChatRoomUser(Map<String, Object> paramMap);

	int updateTokenExp(Map<String, Object> paramMap);

	int updateChatRoomUserTokenIdx(Map<String, Object> paramMap);

	TokenResDto getTokenInfo(Map<String, Object> paramMap);

	int updateChatRoomUserEndDate(Map<String, Object> paramMap);

	int roomPeople(Map<String, Object> dbMap);

	int getSvcInfoCnt(Map<String, Object> dbMap);

	SvcInfoDto getSvcInfo(Map<String, Object> dbMap);

	int getChatRoomUserCount(HashMap<String, Object> dbMap);
	
	int updateChatRoomStatus(Map<String, Object>paramMap);
}