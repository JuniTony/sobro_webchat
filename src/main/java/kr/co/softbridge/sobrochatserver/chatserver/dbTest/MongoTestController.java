package kr.co.softbridge.sobrochatserver.chatserver.dbTest;

import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatUnitDto;
import kr.co.softbridge.sobrochatserver.chatserver.dto.MediaUnitDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/test")
public class MongoTestController {
    @Autowired
    private MongoTest mongoTest;

    @PostMapping("chatunit")
    @ResponseBody
    public List<ChatUnitDto> chatUnit() throws Exception {
        List<ChatUnitDto> unit;
        unit = mongoTest.getChatUnit();
        return unit;
    }



    @PostMapping("mediaunit")
    @ResponseBody
    public List<MediaUnitDto> mediaUnit() {
        List<MediaUnitDto> unit;
        unit = mongoTest.getMediaUnit();
        return unit;
    }
}

