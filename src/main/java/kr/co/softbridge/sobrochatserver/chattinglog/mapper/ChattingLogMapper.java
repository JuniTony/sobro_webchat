package kr.co.softbridge.sobrochatserver.chattinglog.mapper;

import java.util.HashMap;

import kr.co.softbridge.sobrochatserver.chattinglog.dto.ChattingLogDto;
import kr.co.softbridge.sobrochatserver.commons.annotation.PrimaryMapper;

@PrimaryMapper
public interface ChattingLogMapper {

	ChattingLogDto getChattingLog(HashMap<String, Object> paramMap);

	void insertChattingLog(HashMap<String, Object> paramMap);

	void updateChattingLog(HashMap<String, Object> paramMap);

	int getChattingLogCnt(HashMap<String, Object> paramMap);

}
