package kr.co.softbridge.sobrochatserver.chatserver.dbTest;

import groovy.util.logging.Slf4j;
import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatUnitDto;
import kr.co.softbridge.sobrochatserver.chatserver.dto.MediaUnitDto;
import kr.co.softbridge.sobrochatserver.chatserver.mongo.chat.repository.ChatUnitRepository;
import kr.co.softbridge.sobrochatserver.chatserver.mongo.media.repository.MediaUnitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class MongoTest {
    @Autowired
    private ChatUnitRepository chatUnitRepository;
    @Autowired
    private MediaUnitRepository mediaUnitRepository;

    public List<ChatUnitDto> getChatUnit() throws Exception{
        System.out.println(chatUnitRepository.findAll());
        return chatUnitRepository.findAll();
    }

    public List<MediaUnitDto> getMediaUnit() {
        System.out.println(mediaUnitRepository.findAll());
        return mediaUnitRepository.findAll();
    }
}
