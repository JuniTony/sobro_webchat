package kr.co.softbridge.sobrochatserver.chatroom.mapper;

import java.util.HashMap;
import java.util.List;

import kr.co.softbridge.sobrochatserver.commons.annotation.PrimaryMapper;
import kr.co.softbridge.sobrochatserver.commons.dto.MonitoringChatRoomLogDto;
import kr.co.softbridge.sobrochatserver.commons.dto.TokenRoomDto;
import kr.co.softbridge.sobrochatserver.chatroom.dto.ChatRoomCheckDto;
import kr.co.softbridge.sobrochatserver.chatroom.dto.ChatRoomDto;
import kr.co.softbridge.sobrochatserver.chatroom.dto.ChatRoomUserDto;

@PrimaryMapper
public interface ChatRoomMapper {

	List<ChatRoomDto> getRoomList(HashMap<String, Object> paramMap);

	int insertRoom(HashMap<String, Object> paramMap);

	int updateRoom(HashMap<String, Object> paramMap);

	String getRoomCode();

	int checkUpdateRoomUser(HashMap<String, Object> paramMap);

	MonitoringChatRoomLogDto getRoomLogRow(HashMap<String, Object> paramMap);

	TokenRoomDto checkRoomCnt(HashMap<String, Object> dbMap);
	
	String checkRoomManagerId(HashMap<String, Object> dbMap);

	int updateRoomStatus(HashMap<String, Object> paramMap);

	ChatRoomCheckDto getRoomRow(HashMap<String, Object> paramMap);

	int deleteRoom(HashMap<String, Object> delParam);

	void deleteRoomUser(HashMap<String, Object> delParam);

	/* room total count */
	int getRoomTotalCount(HashMap<String, Object> paramMap);

	ChatRoomDto getRoomView(HashMap<String, Object> paramMap);

	List<ChatRoomUserDto> getRoomUser(HashMap<String, Object> paramMap);

	int getRoomUserCount(HashMap<String, Object> paramMap);
	
}

