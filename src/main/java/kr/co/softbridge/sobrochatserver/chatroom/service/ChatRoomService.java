package kr.co.softbridge.sobrochatserver.chatroom.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ChatRoomService {
	
	private static final Logger logger = LogManager.getLogger(ChatRoomService.class);
	
    @Value("${jwt.secret}")
    private String secret;
    
    @Value("${jwt.type.BRO}")
    private String BRO; 

}