package kr.co.softbridge.sobrochatserver.commons.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TokenResDto implements Serializable{

    private String token;
    private Date expDate;
    private String tokenIdx;
    private String siteCode;
    
}
