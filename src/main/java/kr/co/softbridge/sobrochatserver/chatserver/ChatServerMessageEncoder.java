package kr.co.softbridge.sobrochatserver.chatserver;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.google.gson.Gson;

public class ChatServerMessageEncoder implements Encoder.Text<ChatServerMessage> {
	
	private static Gson gson = new Gson();

	@Override
	public String encode(ChatServerMessage message) throws EncodeException {
		return gson.toJson(message);
	}

	@Override
	public void init(EndpointConfig endpointConfig) {
		// custom initialization logic
	}

	@Override
	public void destroy() {
		// close resources
	}
	
}
