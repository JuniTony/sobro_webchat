package kr.co.softbridge.sobrochatserver.chatserver.controller;

import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatServerMessageDto;
import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatServerMessageTypeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
    	System.out.println("none WebSocketEventListener handleWebSocketConnectListener START(-> Received a new web socket connection) !!!!!");
    	System.out.println("11111 event: " + event);
        logger.info("Received a new web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
    	System.out.println("none WebSocketEventListener handleWebSocketDisconnectListener START !!!!!");
    	System.out.println("22222 event: " + event);
    	StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        System.out.println("none WebSocketEventListener handleWebSocketDisconnectListener headerAccessor: " + headerAccessor);
        String username = (String) headerAccessor.getSessionAttributes().get("username");
        System.out.println("none WebSocketEventListener handleWebSocketDisconnectListener username: " + username);
        if(username != null) {
            logger.info("User Disconnected : " + username);

            ChatServerMessageDto chatServerMessageDto = new ChatServerMessageDto();
//            chatServerMessageDto.setType(ChatServerMessageTypeDto.LEAVE);
            chatServerMessageDto.setSender(username);

            messagingTemplate.convertAndSend("/topic/public", chatServerMessageDto);
        }
    }
}