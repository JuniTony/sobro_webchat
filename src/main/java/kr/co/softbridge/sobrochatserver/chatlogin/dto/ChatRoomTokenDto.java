package kr.co.softbridge.sobrochatserver.chatlogin.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoomTokenDto {
	
	private String userNm;
	private String chatRoomCode;
	private String userId;
		
}
