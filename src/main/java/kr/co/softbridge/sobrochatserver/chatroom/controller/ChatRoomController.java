package kr.co.softbridge.sobrochatserver.chatroom.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.co.softbridge.sobrochatserver.chatroom.service.ChatRoomService;
import kr.co.softbridge.sobrochatserver.commons.service.MonitoringLogService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/chatroom")
@RestController
public class ChatRoomController {

	@Value("${chatserver.aes256.key}")
    private String key;
	
    private final ChatRoomService chatRoomService;
    
    private final MonitoringLogService monitoringLogService;
    
}
