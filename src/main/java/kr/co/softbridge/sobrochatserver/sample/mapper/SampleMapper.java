package kr.co.softbridge.sobrochatserver.sample.mapper;

import kr.co.softbridge.sobrochatserver.commons.dto.TokenReqDto;
import kr.co.softbridge.sobrochatserver.commons.dto.SampleDto;
import kr.co.softbridge.sobrochatserver.commons.annotation.PrimaryMapper;

@PrimaryMapper
public interface SampleMapper {

	SampleDto getSampleInfo(TokenReqDto param);

}
