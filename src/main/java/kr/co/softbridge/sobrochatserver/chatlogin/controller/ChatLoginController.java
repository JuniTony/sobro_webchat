package kr.co.softbridge.sobrochatserver.chatlogin.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.co.softbridge.sobrochatserver.chatlogin.service.ChatLoginService;
import kr.co.softbridge.sobrochatserver.commons.service.MonitoringLogService;

/**
 * <pre>
 * sobrochatserver ChatLoginController
 * </pre>
 * 
 * @Author	: user
 * @Date 	: 2022. 5. 18.
 * @Version	: 
 */
@RestController
@RequestMapping("/chatlogin")
public class ChatLoginController {
	private static final Logger logger = LogManager.getLogger(ChatLoginController.class);
        
    @Autowired
    private ChatLoginService chatLoginService;

    @Autowired
    private MonitoringLogService monitoringLogService;
	
}