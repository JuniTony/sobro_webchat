package kr.co.softbridge.sobrochatserver.chatserver.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.softbridge.sobrochatserver.chatlogin.mapper.ChatLoginMapper;
import kr.co.softbridge.sobrochatserver.commons.mapper.CommonTokenMapper;
import kr.co.softbridge.sobrochatserver.commons.service.CommonTokenService;

/**
 * 채팅 서버 service
 */
@Service
public class ChatServerService {
	
	@Value("${jwt.secret}")
	private String secret;
	
	@Value("${jwt.type.BRO}")
    private String BRO;
	
	@Autowired
    private CommonTokenMapper commonTokenMapper;
	
	@Autowired
	private ChatLoginMapper loginMapper;
	
	private static final Logger logger = LogManager.getLogger(ChatServerService.class);
	
	@Autowired
    private CommonTokenService commonTokenService;
   
       
}
