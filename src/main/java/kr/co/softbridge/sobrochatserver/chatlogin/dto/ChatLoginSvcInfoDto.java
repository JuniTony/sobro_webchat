package kr.co.softbridge.sobrochatserver.chatlogin.dto;

import java.util.List;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatLoginSvcInfoDto {

	@ApiParam(value = "응답코드", required = true, example = "000000")
	private String resultCode;
	
	@ApiParam(value = "응답 메세지", required = true, example = "성공")
	private String resultMsg;

}
