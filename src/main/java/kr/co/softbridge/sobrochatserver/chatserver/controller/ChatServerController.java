package kr.co.softbridge.sobrochatserver.chatserver.controller;

import kr.co.softbridge.sobrochatserver.chatserver.ChatServerMessage;
import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatServerMessageDto;
import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatServerMessageTypeDto;
import lombok.RequiredArgsConstructor;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * <pre>
 * 채팅 서버 Controller
 * </pre>
 * 
 * @Author	: user
 * @Date 	: 2022. 5. 18.
 * @Version	: 
 */
@Controller
@RequiredArgsConstructor
public class ChatServerController {

    private final SimpMessageSendingOperations sendingOperations;


    @MessageMapping("/chat/sendMessage")
//    @SendTo("/topic/public")
    public void sendMessage(ChatServerMessageDto chatMessage) throws IOException {
        System.out.println("진입1");
        System.out.println(chatMessage.getRoomNo());
//        chatMessage.setContent(chatMessage.getContent());
        System.out.println(chatMessage.toString());
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        json = mapper.writeValueAsString(chatMessage);
        System.out.println("/sub/roomNo/"+chatMessage.getRoomNo());
        sendingOperations.convertAndSend("/sub/roomNo/"+chatMessage.getRoomNo(), chatMessage);
    }

    @MessageMapping("/chat/addUser")
//    @SendTo("/topic/room")
    public ChatServerMessageDto addUser(@Payload ChatServerMessageDto chatMessage, SimpMessageHeaderAccessor headerAccessor){
        System.out.println("진입2");
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        System.out.println(chatMessage.toString());
        return chatMessage;
    }
}
