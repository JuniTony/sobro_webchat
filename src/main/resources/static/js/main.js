'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;
var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];
let socket2 = null;
let stompTest = null;

$(document).ready(function(){
    console.log("connect")
    connect();
})
function test(event){
    socket2 = new SockJS('/ws')
    stompTest = Stomp.over(socket2)
    console.dir(socket2);
    console.dir(stompTest);
    // stompTest.connect({},sc,err);
    stompTest.connect({},sc);
}
function sc(){
    console.log("sc진입");
    console.log("주석처리 다 했음2")
    stompTest.subscribe(`/sub/roomNo/1234`,err, onMessageReceived)
    // stompTest.subscribe(`/sub/roomNo/1234`)
    stompTest.send("/pub/chat/addUser",
        {},
        JSON.stringify({sender:"park", type: 'JOIN',roomNo:"1234"})
    )

}
function err(err){
    console.log("err 시작 전")
    console.error(err);
}
function connect() {

    if(userId) {
        // usernamePage.classList.add('hidden');
        // chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
        //~ (void) connect(headers, connectCallback, errorCallback)
		//onConnected - connect 콜백 함수
		//onError - error 콜백 함수
    }
    // event.preventDefault();
}


function onConnected() {

    // Subscribe to the Public Topic
    // stompClient.subscribe('/topic/public', onMessageReceived);
    stompClient.subscribe(`/sub/roomNo/${roomNo}`, onMessageReceived);
    //(Object) subscribe(destination, callback, headers = {})
    //명명된 목적지"/topic/public"을 구독합니다. /sub/roomNo/{roomNo} ex)1234

    // Tell your username to the server
    stompClient.send("/pub/chat/addUser",
        {},
        JSON.stringify({sender: userId, type: 'JOIN',roomNo:roomNo})
    )
    //(void) send(destination, headers = {}, body = '')
	//명명된 목적지 "/app/chat.adduser"로 메세지를 보냅니다.
	connectingElement.classList.add('hidden');
}


function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    console.log("뜨는가")
    var messageContent = messageInput.value.trim();
    if(messageContent && stompClient) {
        var chatMessage = {
            roomNo: roomNo,
            sender: userId,
            content: messageInput.value,
            type: 'CHAT'
        };
        stompClient.send("/pub/chat/sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}


function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' 들어옴!';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' 나감!';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

// usernameForm.addEventListener('submit', connect, true);
messageForm.addEventListener('submit', sendMessage, true);