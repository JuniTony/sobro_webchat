package kr.co.softbridge.sobrochatserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = {"kr.co.softbridge.sobrochatserver.chatserver.mongo.media.repository"},
        mongoTemplateRef = MediaMongoConfig.MONGO_TEMPLATE
)
public class MediaMongoConfig {
    protected static final String MONGO_TEMPLATE = "mediaMongoTemplate";
}

