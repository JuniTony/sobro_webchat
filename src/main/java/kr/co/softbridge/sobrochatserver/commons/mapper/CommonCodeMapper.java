package kr.co.softbridge.sobrochatserver.commons.mapper;

import java.util.List;
import java.util.Map;

import kr.co.softbridge.sobrochatserver.commons.annotation.PrimaryMapper;
import kr.co.softbridge.sobrochatserver.commons.dto.CommonCodeDto;
import kr.co.softbridge.sobrochatserver.commons.dto.MonitoringRealTimeChatRoomDto;

@PrimaryMapper
public interface CommonCodeMapper {

	List<CommonCodeDto> getCodeList(String grpCode);

	String getApiServiceNo();

	List<MonitoringRealTimeChatRoomDto> getRealTimeChatRoomList(Map<String, Object> paramMap);

	/* 토탈 카운트 */
	int getRealTimeChatRoomListTotalCount(Map<String, Object> paramMap);

}
