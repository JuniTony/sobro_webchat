package kr.co.softbridge.sobrochatserver.commons.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringRealTimeChatRoomDto {
	@ApiParam(value = "채팅방 코드", required = true, example = "1")
	private	String	chatRoomCode;
	
	@ApiParam(value = "채팅방 참여자 수", required = false, example = "50")
	private	String	onlineUserCnt;
	
	@ApiParam(value = "채팅방 참여자 ID", required = false, example = "50")
	private	String	memberId;
	
	@ApiParam(value = "채팅방 시작일시", required = false, example = "20210101000000")
	private	String	startDt;
	
	@ApiParam(value = "채팅방 종료일시", required = false, example = "20210101235959")
	private	String	endDt;
}
