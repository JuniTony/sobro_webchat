package kr.co.softbridge.sobrochatserver.whiteboard.service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import kr.co.softbridge.sobrochatserver.commons.constants.commonConstant;
import kr.co.softbridge.sobrochatserver.commons.dto.ChatRoomVerifyResponseDto;
import kr.co.softbridge.sobrochatserver.commons.exception.ChatServerException;
import kr.co.softbridge.sobrochatserver.commons.service.CommonTokenService;
import kr.co.softbridge.sobrochatserver.commons.util.StringUtil;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardDeleteDto;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardDto;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardImgDto;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardUpdateDto;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardUploadDto;
import kr.co.softbridge.sobrochatserver.whiteboard.mapper.WhiteboardMapper;

@Service
public class WhiteboardService {
	
	private static final Logger logger	=	LogManager.getLogger(WhiteboardService.class);
	
	@Autowired
	private WhiteboardMapper whiteboardMapper;
	
	@Autowired
	private CommonTokenService commonTokenService;
	
	/*************************************************************************************************************
	* @brief  : 화이트보드 이미지 정보 저장 Service
	* @method : uploadWhiteboard
	* @author : user
	**************************************************************************************************************/
	public ResponseEntity<WhiteboardUploadDto> uploadWhiteboard(HttpServletRequest request, HashMap<String, Object> paramMap) {
		
		int 	imgId	= 	0;
		String	userId	= 	"";
		String	userNm	=	"";
		
		try {
			
			//01.넘어온 Parameter 검증
			if(StringUtil.isEmpty(String.valueOf(paramMap.get("roomCode")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("roomToken")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("widthRatio")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("heightRatio")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("xCoordinateRatio")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("yCoordinateRatio")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("userId")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("userNm")))
			) {
				throw new ChatServerException("000001", 	commonConstant.M_000001);
			}
			
			//룸토큰 검증
			ChatRoomVerifyResponseDto roomVerifyResponseDto = commonTokenService.getChatRoomTokenCheck(paramMap);
			if(!"000000".equals(roomVerifyResponseDto.getResultCode())) {
				throw new ChatServerException("005006", 	commonConstant.M_005006);
			}
			
			//IMG 정보 저장
			
			//S3파일 업로드
			
			//파일URL, PATH paramMap 주입
			
			//이미지 아이디 생성
			imgId = getImageId();
			paramMap.put("imageId", imgId);
			
			int	insImgCnt	=	whiteboardMapper.insertWhiteboard(paramMap);
			if(insImgCnt < 0) {
				throw new ChatServerException("004001",		commonConstant.M_004001);
			}else {
				userId		=		String.valueOf(paramMap.get("userId"));
				userNm		=		String.valueOf(paramMap.get("userNm"));
			}
		}catch (ChatServerException e) {
    		if(StringUtil.isNotEmpty(e.getErrorLogMsg())) {
				logger.info("[uploadWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getErrorLogMsg());
			}else {
				logger.info("[uploadWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getMessage());
			}
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardUploadDto
	    					.builder()
	    					.resultCode(e.getErrorCode())
	    					.resultMsg(e.getMessage())
	    					.build());
			//
    	}
		catch(Exception e) {
			logger.info("000003", commonConstant.M_000003, e.getMessage());
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardUploadDto
	    					.builder()
	    					.resultCode("000003")
	    					.resultMsg(commonConstant.M_000003)
	    					.build());
		}
		//
		return ResponseEntity.status(HttpStatus.OK)
    			.body(WhiteboardUploadDto
    					.builder()
    					.resultCode("000000")
    					.resultMsg(commonConstant.M_000000)
    					.imgId(imgId)
    					.userId(userId)
    					.userNm(userNm)
    					.build());
	}
	
	/*************************************************************************************************************
	* @brief  : 화이트보드 이미지 정보 수정 Service
	* @method : updateWhiteboard
	* @author : user
	**************************************************************************************************************/
	public ResponseEntity<WhiteboardUpdateDto> updateWhiteboard(HttpServletRequest request, HashMap<String, Object> paramMap) {
		
		int		imgId	=	0;
		String	userId	= 	"";
		String	userNm	= 	"";
		
		try {
			
			//01.넘어온 Parameter 검증
			if(
					StringUtil.isEmpty(String.valueOf(paramMap.get("roomCode")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("roomToken")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("imgId")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("widthRatio")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("heightRatio")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("xCoordinateRatio")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("yCoordinateRatio")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("userId")))
					|| StringUtil.isEmpty(String.valueOf(paramMap.get("userNm")))
					) {
					throw new ChatServerException("000001", 	commonConstant.M_000001);
			}
			
			//룸토큰 검증
			ChatRoomVerifyResponseDto roomVerifyResponseDto = commonTokenService.getChatRoomTokenCheck(paramMap);
			if(!"000000".equals(roomVerifyResponseDto.getResultCode())) {
				throw new ChatServerException("005006" , 		commonConstant.M_005006);
			}
			
			//02.IMG 정보 저장
			int uptImgCnt = whiteboardMapper.updateWhiteboard(paramMap);
			if(uptImgCnt < 0) {
				throw new ChatServerException("004002", 		commonConstant.M_004002);
			}else {
				userId		=		String.valueOf(paramMap.get("userId"));
				userNm		=		String.valueOf(paramMap.get("userNm"));
				imgId		=		Integer.parseInt(String.valueOf(paramMap.get("imgId")));
			}
		}catch (ChatServerException e) {
    		if(StringUtil.isNotEmpty(e.getErrorLogMsg())) {
				logger.info("[updateWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getErrorLogMsg());
			}else {
				logger.info("[updateWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getMessage());
			}
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardUpdateDto
	    					.builder()
	    					.resultCode(e.getErrorCode())
	    					.resultMsg(e.getMessage())
	    					.build());
		}
		catch(Exception e) {
			logger.info("000003", commonConstant.M_000003, e.getMessage());
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardUpdateDto
	    					.builder()
	    					.resultCode("000003")
	    					.resultMsg(commonConstant.M_000003)
	    					.build());
		}
		
		return ResponseEntity.status(HttpStatus.OK)
    			.body(WhiteboardUpdateDto
    					.builder()
    					.resultCode("000000")
    					.resultMsg(commonConstant.M_000000)
    					.imgId(imgId)
    					.userId(userId)
    					.userNm(userNm)
    					.build());
	}
	
	/*************************************************************************************************************
	* @brief  : 화이트보드 이미지 정보 삭제 Service
	* @method : deleteWhiteboard
	* @author : user
	**************************************************************************************************************/
	public ResponseEntity<WhiteboardDeleteDto> deleteWhiteboard(HttpServletRequest request, HashMap<String, Object> paramMap) {
		
		String	roomCode	=	"";
		String	userId		= 	"";
		String	userNm		= 	"";
		
		try {
			
			//01.넘어온 Parameter 검증
			if(StringUtil.isEmpty(String.valueOf(paramMap.get("roomCode")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("roomToken")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("userId")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("userNm")))
			) {
				throw new ChatServerException("000001",		commonConstant.M_000001);
			}
			
			//룸토큰 검증
			ChatRoomVerifyResponseDto roomVerifyResponseDto = commonTokenService.getChatRoomTokenCheck(paramMap);
			if(!"000000".equals(roomVerifyResponseDto.getResultCode())) {
				throw new ChatServerException("005006" , 	commonConstant.M_005006);
			}
			
			//02.화이트보드 이미지 정보 삭제
			int delImgCnt = whiteboardMapper.deleteWhiteboard(paramMap);
			if(delImgCnt < 0) {
				throw new ChatServerException("004003",		commonConstant.M_004003);
			}else {
				roomCode	=	String.valueOf(paramMap.get("roomCode"));
				userId		=	String.valueOf(paramMap.get("userId"));
				userNm		=	String.valueOf(paramMap.get("userNm"));
			}
		}catch (ChatServerException e) {
    		if(StringUtil.isNotEmpty(e.getErrorLogMsg())) {
				logger.info("[deleteWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getErrorLogMsg());
			}else {
				logger.info("[deleteWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getMessage());
			}
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardDeleteDto
	    					.builder()
	    					.resultCode(e.getErrorCode())
	    					.resultMsg(e.getMessage())
	    					.build());
		}
		catch(Exception e) {
			logger.info("000003", commonConstant.M_000003, e.getMessage());
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardDeleteDto
	    					.builder()
	    					.resultCode("000003")
	    					.resultMsg(commonConstant.M_000003)
	    					.build());
		}
		return ResponseEntity.status(HttpStatus.OK)
    			.body(WhiteboardDeleteDto
    					.builder()
    					.resultCode("000000")
    					.resultMsg(commonConstant.M_000000)
    					.roomCode(roomCode)
    					.userId(userId)
    					.userNm(userNm)
    					.build());
	}
	
	/*************************************************************************************************************
	* @brief  : 화이트보드 이미지 정보 조회 Service
	* @method : selectWhiteboard
	* @author : user
	**************************************************************************************************************/
	public ResponseEntity<WhiteboardDto> selectWhiteboard(HttpServletRequest request, HashMap<String, Object> paramMap) {
		List<WhiteboardImgDto>	whiteboardList	=	null;
		
		try {
			//01.넘어온 Parameter 검증
			if(StringUtil.isEmpty(String.valueOf(paramMap.get("roomCode")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("roomToken")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("userId")))
			|| StringUtil.isEmpty(String.valueOf(paramMap.get("userNm")))
			) {
				throw new ChatServerException("000001",	 	commonConstant.M_000001);
			}
			
			//룸토큰 검증
			ChatRoomVerifyResponseDto roomVerifyResponseDto = commonTokenService.getChatRoomTokenCheck(paramMap);
			if(!"000000".equals(roomVerifyResponseDto.getResultCode())) {
				throw new ChatServerException("005006",		commonConstant.M_005006);
			}
			
			//02.이미지 정보 조회
			whiteboardList = whiteboardMapper.selectWhiteboard(paramMap);
		}catch (ChatServerException e) {
    		if(StringUtil.isNotEmpty(e.getErrorLogMsg())) {
				logger.info("[selectWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getErrorLogMsg());
			}else {
				logger.info("[selectWhiteboard] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getMessage());
			}
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardDto
	    					.builder()
	    					.resultCode(e.getErrorCode())
	    					.resultMsg(e.getMessage())
	    					.build());
		}
		catch(Exception e) {
			logger.info("000003", commonConstant.M_000003, e.getMessage());
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(WhiteboardDto
	    					.builder()
	    					.resultCode("000003")
	    					.resultMsg(commonConstant.M_000003)
	    					.build());
		}
		return ResponseEntity.status(HttpStatus.OK)
    			.body(WhiteboardDto
    					.builder()
    					.resultCode("000000")
    					.resultMsg(commonConstant.M_000000)
    					.whiteboardList(whiteboardList)
    					.build());
	}
	
	//화이트보드 이미지 아이디 생성
	public int getImageId() {
		return whiteboardMapper.getWhiteboardId();
	}
	
}
