package kr.co.softbridge.sobrochatserver.chatserver.controller;

import kr.co.softbridge.sobrochatserver.chatserver.dbTest.MongoTest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/chat")
public class ChatRooomController {
    private final MongoTest mongoTest;

    @PostMapping("/room")
    public String rooms(@RequestParam(required = false) String roomNo,
                        @RequestParam(required = false) String name,
                        Model model) {
        model.addAttribute("roomNo", roomNo);
        model.addAttribute("userId", name);
        System.out.println(model.getAttribute("roomNo"));
        System.out.println(model.getAttribute("userId"));
        return "chat/room";
    }
}
