package kr.co.softbridge.sobrochatserver.whiteboard.mapper;

import java.util.HashMap;
import java.util.List;

import kr.co.softbridge.sobrochatserver.commons.annotation.PrimaryMapper;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardImgDto;

@PrimaryMapper
public interface WhiteboardMapper {
	
	//화이트보드 저장 mapper
	int insertWhiteboard(HashMap<String, Object> paramMap);
	
	//화이트보드 ID 조회
	int getWhiteboardId(HashMap<String, Object> paramMap);
	
	//화이트보드 수정 mapper
	int updateWhiteboard(HashMap<String, Object> paramMap);
	
	//화이트보드 삭제 mapper
	int deleteWhiteboard(HashMap<String, Object> paramMap);
	
	//화이트보드 조회 mapper
	List<WhiteboardImgDto> selectWhiteboard(HashMap<String, Object> paramMap);

	//화이트보드 아이디 생성
	int getWhiteboardId();
		
}
