package kr.co.softbridge.sobrochatserver.chatroom.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatRoomUserDto {

	@ApiParam(value = "사용자ID", required = false, example = "user01")
	private String userId;
	
	@ApiParam(value = "사용자이름", required = false, example = "사용자")
	private String userNm;
}
