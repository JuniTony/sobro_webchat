package kr.co.softbridge.sobrochatserver.commons.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import kr.co.softbridge.sobrochatserver.commons.constants.commonConstant;
import kr.co.softbridge.sobrochatserver.commons.dto.ChatRoomVerifyResponseDto;
import kr.co.softbridge.sobrochatserver.commons.dto.MonitoringApiCallLogDto;
import kr.co.softbridge.sobrochatserver.commons.dto.MonitoringRealTimeChatRoomDto;
import kr.co.softbridge.sobrochatserver.commons.dto.MonitoringRealTimeChatRoomResDto;
import kr.co.softbridge.sobrochatserver.commons.mapper.CommonCodeMapper;
import kr.co.softbridge.sobrochatserver.commons.util.StringUtil;
import kr.co.softbridge.sobrochatserver.config.jwt.JwtUtil;

@Service
public class MonitoringLogService {

	private static final Logger logger = LogManager.getLogger(MonitoringLogService.class);

    @Value("${jwt.secret}")
    private String secret; 
    
    @Autowired
    private CommonCodeMapper commonCodeMapper;
    
    @Autowired
    private CommonTokenService commonTokenService;

	public void chatRoomRealTimeLog(HttpServletResponse requestHeader, Map<String, Object> paramMap) {
		JwtUtil jwtUtil = new JwtUtil(secret);
		
		try {
			logger.info("[chatRoomRealTimeLog] paramMap=[" + paramMap.toString() + "]");
			// 토큰검증 로직추가영역
			ChatRoomVerifyResponseDto chatRoomTokenInfo = commonTokenService.getChatRoomTokenCheck(paramMap);
			if("000000".equals(chatRoomTokenInfo.getResultCode())) {
				// paramMap 필요변수 정의
				/* roomToken 검증추가시 토큰정보로 대체될 변수 start */
				String	svcCode				= chatRoomTokenInfo.getSvcCode();
				String	chatRoomCode		= (String) paramMap.get("chatRoomCode");
				String	userId				= (String) paramMap.get("userId");
				/* roomToken 검증추가시 토큰정보로 대체될 변수 end */
				String	videoBitrate		= StringUtil.null2void(paramMap.get("videoBitrate"), "0bps");
				String	videoWidth			= StringUtil.null2void(paramMap.get("videoWidth"), "0px");
				String	videoHeight			= StringUtil.null2void(paramMap.get("videoHeight"), "0px");
				String	videoFrameRate		= StringUtil.null2void(paramMap.get("videoFrameRate"), "0bps");
				String	videoAspectRatio	= StringUtil.null2void(paramMap.get("videoAspectRatio"), "0");
				String	audioBitrate		= StringUtil.null2void(paramMap.get("audioBitrate"), "0bps");
				String	audioLaency			= StringUtil.null2void(paramMap.get("audioLaency"), "0sec");
				String	audioSampleRate		= StringUtil.null2void(paramMap.get("audioSampleRate"), "0");
				String	audioSampleSize		= StringUtil.null2void(paramMap.get("audioSampleSize"), "0");
				
				JSONObject consoleMsg	= new JSONObject();
				
				consoleMsg.put("svcCode",			svcCode);
				consoleMsg.put("chatRoomCode",		chatRoomCode);
				consoleMsg.put("userId",			userId);
				consoleMsg.put("videoBitrate",		videoBitrate);
				consoleMsg.put("videoWidth",		videoWidth);
				consoleMsg.put("videoHeight",		videoHeight);
				consoleMsg.put("videoFrameRate",	videoFrameRate);
				consoleMsg.put("videoAspectRatio",	videoAspectRatio);
				consoleMsg.put("audioBitrate",		audioBitrate);
				consoleMsg.put("audioLaency",		audioLaency);
				consoleMsg.put("audioSampleRate",	audioSampleRate);
				consoleMsg.put("audioSampleSize",	audioSampleSize);
							
				System.out.println("quality: "+consoleMsg.toString());
			}
		} catch(Exception e) {
			logger.info("[chatRoomRealTimeLog] " + e.getMessage());
			e.printStackTrace();
			//throw new RuntimeException();
		}
	}
	
	public void chatRoomLog(String logName, Object obj) {
		
		try {
			JSONObject jo = StringUtil.objectToJson(obj);
						
			System.out.println(logName + ": " + jo.toString());
		} catch(Exception e) {
			logger.info("[chatRoomLog] " + e.getMessage());
			e.printStackTrace();
			//throw new RuntimeException();
		}
	}
	
	public void apiCallLog(MonitoringApiCallLogDto apiDto) {
		try {
			String apiServiceNo = getApiServiceNo();
			apiDto.setApiServiceNo(apiServiceNo);
			JSONObject jo = StringUtil.objectToJson(apiDto);
						
			System.out.println("apiInfo: " + jo.toString());
		} catch(Exception e) {
			logger.info("[apiCallLog apiDto] " + e.getMessage());
			e.printStackTrace();
			//throw new RuntimeException();
		}
	}
	
	public void apiCallLog(
			String startDate
			, String endDate
			, String apiServiceNm
			, String apiReqServiceCode
			, String reqResultCode
			, HashMap<String, Object> param
			, ResponseEntity result) {
		try {
			MonitoringApiCallLogDto macld = new MonitoringApiCallLogDto();
	    	macld.setApiServiceNm(apiServiceNm);
	    	macld.setApiReqServiceCode(apiReqServiceCode);
	    	macld.setSvcCode((String) param.get("svcCode"));
	    	macld.setSvcNm(commonConstant.SERVICE_NAME);
	    	macld.setReqResultCode(reqResultCode);
	    	macld.setReqTime(startDate);
	    	if (param != null) {
	    		macld.setReqText("request: "+StringUtil.objectToJson(param).toString().replaceAll("\\\"", ""));
	    	}
	    	macld.setResTime(endDate);
	    	if (result != null) {
	    		macld.setResText("response: "+StringUtil.objectToJson(result.getBody()).toString().replaceAll("\\\"", ""));
	    	}
	    	
			String apiServiceNo = getApiServiceNo();
			macld.setApiServiceNo(apiServiceNo);
			JSONObject jo = StringUtil.objectToJson(macld);
						
			System.out.println("apiInfo: " + jo.toString());
		} catch(Exception e) {
			logger.info("[apiCallLog] " + e.getMessage());
			e.printStackTrace();
			//throw new RuntimeException();
		}
	}
	
	public String getApiServiceNo() {
		
		SecureRandom random = new SecureRandom();
		int rdmInt = random.nextInt(9);
		String pin = "";
		String pattern = "YYYYMMDDHHmmssSSS";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String setTime = simpleDateFormat.format(new Date());
    	while (pin.length() < 2) {
		    rdmInt = random.nextInt(9);
		    String addition = String.valueOf(rdmInt);
		    if (pin.contains(addition)) continue;
		    pin += addition;
		}
		pin = "ASN" + setTime + pin;
		
		return pin;
	}

	public ResponseEntity<MonitoringRealTimeChatRoomResDto> getChatRoomList(Map<String, Object> paramMap) throws URISyntaxException {
		List<MonitoringRealTimeChatRoomDto> roomList = null;
		int totalCount = 0;
		try {
			
			totalCount = commonCodeMapper.getRealTimeChatRoomListTotalCount(paramMap);
			
			int pageNum = Integer.parseInt(StringUtil.null2void(paramMap.get("page"), "1"));
			int pageSize = Integer.parseInt(StringUtil.null2void(paramMap.get("pageSize"), "50"));
			if(pageNum == 1) {
				pageNum = pageNum - 1;
			}else {
				pageNum = (pageNum -1) * pageSize;
			}
			paramMap.put("pageNum", pageNum);
			paramMap.put("pageSize", pageSize);
			roomList = commonCodeMapper.getRealTimeChatRoomList(paramMap);
		}catch (Exception e) {
			logger.info("000003", commonConstant.M_000003, e.getMessage());
			return ResponseEntity.status(HttpStatus.OK)
					.body(MonitoringRealTimeChatRoomResDto
							.builder()
							.resultCode("000003")
							.resultMsg(commonConstant.M_000003)
							.build());
		}
		return ResponseEntity.created(new URI("/realTimeChatRoomList"))
				.body(MonitoringRealTimeChatRoomResDto
						.builder()
						.resultCode("000000")
						.resultMsg(commonConstant.M_000000)
						.roomList(roomList)
						.build());
	}
	
}
