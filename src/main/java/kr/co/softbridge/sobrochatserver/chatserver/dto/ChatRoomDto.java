package kr.co.softbridge.sobrochatserver.chatserver.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChatRoomDto {
    private String roomNo;
    private String userId;
}
