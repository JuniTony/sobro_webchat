package kr.co.softbridge.sobrochatserver.chatserver.mapper;

import java.util.HashMap;
import java.util.List;

import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatServerListDto;
import kr.co.softbridge.sobrochatserver.commons.annotation.SecondaryMapper;

/**
 * 채팅 서버 mapper
 */
@SecondaryMapper
public interface ChatServerMapper {

    /**
     * 다음 파일 시퀀스 조회
     * @param room_code
     * @return 다음 파일 시퀀스
     */
    Integer getNextSequenceByRoomCode(String room_code);

    /**
     * 파일 목록 조회
     * @param ChatServerListDto
     * @return
     */
	List<ChatServerListDto> getChatServerList(HashMap<String, Object> paramMap);

	/* 파일 리스트 토탈 카운트 */
	int getChatServerListCount(HashMap<String, Object> paramMap);
	
}
