package kr.co.softbridge.sobrochatserver.chatroom.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoomResponeDao {
	
	@ApiParam(value = "채팅방 코드", required = true, example = "2000120112")
	private String chatRoomcode;
	
	@ApiParam(value = "응답 코드", required = true, example = "000000")
    private String resultCode;
	
	@ApiParam(value = "응답 메시지", required = true, example = "성공")
    private String resultMsg;
	
}
