package kr.co.softbridge.sobrochatserver.chatserver.dto;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "unit")
/*************************************************************************************************************
 * @FileName : unitDto.java
 * @프로그램설명 : 미디어서버 몽고디비 unit 도큐먼트 매핑 DTO
 **************************************************************************************************************/
public class MediaUnitDto {
    private String domain;
    private int wss_port;
    private String private_ip;
    private String public_ip;
    private int tcp_port;
    private int unit_fd;
    private int RoomCode;
}
