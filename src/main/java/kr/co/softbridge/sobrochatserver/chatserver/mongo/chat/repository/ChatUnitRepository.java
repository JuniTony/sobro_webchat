package kr.co.softbridge.sobrochatserver.chatserver.mongo.chat.repository;

import kr.co.softbridge.sobrochatserver.chatserver.dto.ChatUnitDto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChatUnitRepository extends MongoRepository<ChatUnitDto, String> {
}
