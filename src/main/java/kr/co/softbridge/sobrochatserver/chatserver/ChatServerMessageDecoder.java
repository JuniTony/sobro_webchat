package kr.co.softbridge.sobrochatserver.chatserver;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import com.google.gson.Gson;

public class ChatServerMessageDecoder implements Decoder.Text<ChatServerMessage> {

	private static Gson gson = new Gson();

	@Override
	public ChatServerMessage decode(String s) throws DecodeException {
		return gson.fromJson(s, ChatServerMessage.class);
	}

	@Override
	public boolean willDecode(String s) {
		return (s != null);
	}

	@Override
	public void init(EndpointConfig endpointConfig) {
		// custom initialization logic
	}

	@Override
	public void destroy() {
		// close resources
	}

}
