package kr.co.softbridge.sobrochatserver.chatroom.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ChatRoomViewDto {

	@ApiParam(value = "응답 코드", required = true, example = "000000")
    private String resultCode;
	
	@ApiParam(value = "응답 메시지", required = true, example = "성공")
    private String resultMsg;
	
	@ApiParam(value = "사용자 정보", required = true, example = "1")
    private ChatRoomDto roomInfo;
	
	@ApiParam(value = "사용자 명수", required = true, example = "1")
    private int userCnt;
	
	@ApiParam(value = "사용자 정보", required = true, example = "1")
    private List<ChatRoomUserDto> userList;
	
}
