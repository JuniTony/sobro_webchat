package kr.co.softbridge.sobrochatserver.chatlogin.service;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.softbridge.sobrochatserver.chatlogin.dto.ChatLiveUserDto;
import kr.co.softbridge.sobrochatserver.chatlogin.mapper.ChatLoginMapper;
import kr.co.softbridge.sobrochatserver.commons.service.MonitoringLogService;

@Service
@Transactional
public class ChatLoginService {

	private static final Logger logger = LogManager.getLogger(ChatLoginService.class);
	
    @Value("${jwt.secret}")
    private String secret; 
    
    @Value("${jwt.type.BRO}")
    private String BRO; 
    
    @Value("${jwt.type.USER}")
    private String USER; 

	@Autowired
    private ChatLoginMapper loginMapper;

	@Autowired
	private MonitoringLogService monitoringLogService;
	
	public ChatLiveUserDto getChatRoomUserInfo(Map<String, Object> paramMap) {
		ChatLiveUserDto returnDto = new ChatLiveUserDto();
		Boolean roomUser = false;
		String userId = (String) paramMap.get("userId");
		
		if (userId == null || "".equals(userId)) {
			SecureRandom random = new SecureRandom();
			int rdmInt = random.nextInt(9);
			String pin = "";
			String pattern = "YYYYMMDDHHmmssSSS";
	    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	    	String setTime = simpleDateFormat.format(new Date());
	    	while (pin.length() < 4) {
			    rdmInt = random.nextInt(9);
			    String addition = String.valueOf(rdmInt);
			    if (pin.contains(addition)) continue;
			    pin += addition;
			}
			pin = "RU" + setTime + pin;
			
			userId = pin;
			paramMap.put("userId", userId);
		}
		returnDto = loginMapper.getChatRoomUser(paramMap);

		return returnDto;
	}

}