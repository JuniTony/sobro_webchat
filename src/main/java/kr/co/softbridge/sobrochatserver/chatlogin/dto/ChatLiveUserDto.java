package kr.co.softbridge.sobrochatserver.chatlogin.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatLiveUserDto {
	
	@ApiParam(value = "사용자 ID", required = true, example = "abc1234")
    private String userId;
    
	@ApiParam(value = "채팅방 번호", required = true, example = "1234")
    private String chatRoomCode;
    
	@ApiParam(value = "사용자명", required = false, example = "홍길동")
    private String userNm;
    
	@ApiParam(value = "사용자레벨", required = false, example = "1001")
    private String userLevel;
	
	@ApiParam(value = "최대 참여자 수", required = false, example = "50")
	private	int		maxPeople;
    
	@ApiParam(value = "채팅방입장토큰인덱스", required = true, example = "1")
    private String tokenIdx;
    
	@ApiParam(value = "채팅방입장토큰", required = true, example = "AES256")
    private String chatRoomToken;

	@ApiParam(value = "응답 코드", required = true, example = "000000")
    private String resultCode;
	
	@ApiParam(value = "응답 메시지", required = true, example = "성공")
    private String resultMsg;

}
