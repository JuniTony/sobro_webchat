package kr.co.softbridge.sobrochatserver.chatroom.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoomDto {
	
	@ApiParam(value = "채팅방 코드", required = true, example = "1")
    private String chatRoomCode;
	
	@ApiParam(value = "최대 참여자 수", required = true, example = "100")
	private String maxPeople;
	
	@ApiParam(value = "채팅방 시작날짜", required = true, example = "20210101000000")
    private String startDt;
	
	@ApiParam(value = "채팅방 종료날짜", required = true, example = "20210102000000")
    private String endDt;
	
	@ApiParam(value = "관리자ID", required = true, example = "streamer1")
	private String managerId;
	
	@ApiParam(value = "공동관리자ID", required = false, example = "streamer2")
	private String partnerId;
	
	@ApiParam(value = "채팅방 비밀번호", required = true, example = "1234")
    private String chatRoomJoinCode;
	
	@ApiParam(value = "채팅방 상태", required = true, example = "종료")
    private String chatRoomStatus;
	
	@ApiParam(value = "서비스코드", required = true, example = "종료")
    private String svcCode;
	
	public String toStringShortPrefix() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
