package kr.co.softbridge.sobrochatserver.config;

import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
public class MultipleMongoConfig {
    //
    @Primary
    @Bean(name = "mediaMongoDb")
    @ConfigurationProperties(prefix = "spring.data.mongodb.primary")
    public MongoProperties getMediaDbProps() throws Exception {
        return new MongoProperties();
    }
    //
    @Bean(name = "chatMongoDb")
    @ConfigurationProperties(prefix = "spring.data.mongodb.secondary")
    public MongoProperties getChatDbProps() throws Exception {
        return new MongoProperties();
    }
    //
    @Primary
    @Bean(name = "mediaMongoTemplate")
    public MongoTemplate mediaMongoTemplate() throws Exception {
        return new MongoTemplate(mediaMongoDatabaseFactory(getMediaDbProps()));
    }
    //
    @Bean(name = "chatMongoTemplate")
    public MongoTemplate chatMongoTemplate() throws Exception {
        return new MongoTemplate(chatMongoDatabaseFactory(getChatDbProps()));
    }
    //
    @Primary
    @Bean
    public MongoDatabaseFactory mediaMongoDatabaseFactory(MongoProperties mongo) throws Exception {
        return new SimpleMongoClientDatabaseFactory(
                mongo.getUri()
        );
    }
    //
    @Bean
    public MongoDatabaseFactory chatMongoDatabaseFactory(MongoProperties mongo) throws Exception {
        return new SimpleMongoClientDatabaseFactory(
                mongo.getUri()
        );
    }

}
