//package kr.co.softbridge.sobrochatserver.config;
//
////import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.context.annotation.Configuration;
////import org.springframework.messaging.simp.config.MessageBrokerRegistry;
////import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
////import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
////import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
////import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
//
////Profile("!stomp")
////@Configuration
////@EnableWebSocketMessageBroker	//@EnableWebSocketMessageBroker is used to enable our WebSocket server
//public class WebSocketConfig {	// implements WebSocketMessageBrokerConfigurer {
//
////	@Autowired
////    private ChatHandler chatHandler;
////	
////	@Override
////    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
////        // 해당 endpoint로 handshake가 이루어진다.
////        registry.addHandler(chatHandler, "/ws/chat").setAllowedOrigins("*").withSockJS();
////    }
//	
//}

////////////////////////////////////////////////////////

package kr.co.softbridge.sobrochatserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker	//@EnableWebSocketMessageBroker is used to enable our WebSocket server
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
      registry.addEndpoint("/ws").setAllowedOriginPatterns("*");
      registry.addEndpoint("/ws").setAllowedOriginPatterns("*").withSockJS();
      /*
       * withSockJS()
       * 는 웹소켓을 지원하지 않는 브라우저에
       * 폴백 옵션을 활성화하는데 사용됩니다.
       *
       **/
  }

  /*@Override
  public void configureMessageBroker(MessageBrokerRegistry registry) {
      registry.setApplicationDestinationPrefixes("/app");
//      registry.enableSimpleBroker("/topic");
      //
      registry.enableSimpleBroker("/queue", "/topic","/roomNo");
  }*/
  @Override
  public void configureMessageBroker(MessageBrokerRegistry registry) {
      // 메세지 발송시 /pub/~~~ 로 보낼 예정
      registry.setApplicationDestinationPrefixes("/pub");
      // 클라이언트 사용자의 구독경로 /sub/roomNo/룸 넘버
      registry.enableSimpleBroker("/sub","/topic");
  }
}

