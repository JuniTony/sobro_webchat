package kr.co.softbridge.sobrochatserver.chatserver.mongo.media.repository;

import kr.co.softbridge.sobrochatserver.chatserver.dto.MediaUnitDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
/*************************************************************************************************************
 * @FileName : UnitRepository
 * @프로그램설명 : 몽고디비 unit 도큐먼트 repository
 **************************************************************************************************************/
public interface MediaUnitRepository extends MongoRepository<MediaUnitDto, String> {
}
