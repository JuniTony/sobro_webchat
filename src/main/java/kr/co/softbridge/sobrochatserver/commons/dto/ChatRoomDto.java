package kr.co.softbridge.sobrochatserver.commons.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoomDto {
	
	@ApiParam(value = "채팅방 코드", required = true, example = "1")
	private	String	chatRoomCode;
	
	@ApiParam(value = "관리자 ID", required = false, example = "streamer1")
	private	String	managerId;
	
	@ApiParam(value = "공동관리자 ID", required = false, example = "partner2")
	private	String	partnerId;
	
	@ApiParam(value = "채팅방 시작일시", required = false, example = "20210101000000")
	private	String	startDt;
	
	@ApiParam(value = "채팅방 종료일시", required = false, example = "20210101235959")
	private	String	endDt;
	
	@ApiParam(value = "노출 여부", required = false, example = "Y|N")
	private	String	viewYn;

	@ApiParam(value = "입장승인 여부", required = false, example = "Y|N")
	private	String	joinYn;
	
	@ApiParam(value = "채팅방 비밀번호", required = false, example = "AES256")
	private	String	joinCode;
	
	@ApiParam(value = "채팅방 상태", required = false, example = "01")
	private	String	chatRoomStatus;
	
	@ApiParam(value = "예약시간도래전여부", required = false, example = "Y")
	private String startArrival;
	
	@ApiParam(value = "방 종료 시간 여부", required = false, example = "Y")
	private String endArrival;
	
}

