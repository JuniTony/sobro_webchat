package kr.co.softbridge.sobrochatserver.commons.exception;

public class ChatServerException extends Exception{
	
	private String errorCode = "";
	private String errorLogMsg = "";
	
	public ChatServerException(String code, String msg, String msg2) {
		super(msg);
		errorCode = code;
		errorLogMsg = msg2;
	}
	
	public ChatServerException(String code, String msg) {
		super(msg);
		errorCode = code;
	}
	
	public ChatServerException(String msg) {
		super(msg);
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public String getErrorLogMsg() {
		return errorLogMsg;
	}

}
