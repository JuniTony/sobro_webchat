package kr.co.softbridge.sobrochatserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = {"kr.co.softbridge.sobrochatserver.chatserver.mongo.chat.repository"},
        mongoTemplateRef = ChatMongoConfig.MONGO_TEMPLATE
)
public class ChatMongoConfig {
    protected static final String MONGO_TEMPLATE = "chatMongoTemplate";
}
