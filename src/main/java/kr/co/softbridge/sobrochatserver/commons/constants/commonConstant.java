package kr.co.softbridge.sobrochatserver.commons.constants;

public class commonConstant {
	
	// 사용자 권한
	public static String SERVICE_ADMIN				= "A";			// 관리자
	public static String SERVICE_MANAGER			= "M";			// 방송매니저
	public static String SERVICE_USER				= "U";			// 사용자
	
	public static String SERVICE_NAME				= "채팅 서버";
	
	// 채팅 서버 접속 사용자 LEVEL
	public static String LEVEL_MANAGER				= "1001";		// 매니저
	public static String LEVEL_USER					= "2001";		// 사용자
	
	
	/* 채팅 서버 리스트 코드상수 */
	// 채팅 서버 상태
	public static String ROOM_STATUS_WAITING		= "01";			// 대기
	public static String ROOM_STATUS_RUNNING		= "02";			// 진행중
	public static String ROOM_STATUS_CLOSE			= "03";			// 종료
	
	// 채팅 서버 저장 타입
	public static String SAVE_CRATE					 = "C";			// 등록
	public static String SAVE_UPDATE				 = "U";			// 수정
	public static String SAVE_DELETE				 = "D";			// 삭제
	
	
	/*
	 * 에러메세지 처리
	 */
	//공통
	public static String M_000000					= "성공";
	public static String M_000001					= "필수 정보 누락 되었습니다.";
	public static String M_000002					= "올바른 입력값이 아닙니다.";
	public static String M_000003					= "SQL Exception이 발생하였습니다.";
	public static String M_000004					= "서비스 토큰 생성을 실패 하였습니다.";
	
	//화상회의 관리
	public static String M_001001					= "채팅 정보가 없습니다.";
	public static String M_001002					= "채팅 생성을 실패 하였습니다.";
	public static String M_001003					= "채팅 수정을 실패 하였습니다.";
	public static String M_001004					= "채팅 삭제를 실패 하였습니다.";
	public static String M_001005					= "채팅 수정 권한이 없습니다.";
	public static String M_001006					= "채팅 삭제 권한이 없습니다.";
	public static String M_001007					= "채팅 참석 인원을 초과하였습니다.";
	public static String M_001008					= "채팅 참여에 실패하였습니다.";
	public static String M_001009					= "이미 존재하는 사용자명입니다";
	public static String M_001010					= "사용자 정보를 찾을 수 없습니다.";
	public static String M_001011					= "채팅 입장 비밀번호를 확인해 주세요.";
	public static String M_001012					= "상태변경을 할 수 없는 채팅입니다.";
	public static String M_001013					= "종료된 채팅입니다.";
	public static String M_001014					= "진행 중인 채팅가 아닙니다.";
	public static String M_001015					= "닉네임은 최대 한글 1~10자, 영문 및 숫자 2~20자 입니다.";
	public static String M_001016					= "사용자 정보 수정 실패하였습니다.";
	
	//파일관리
	public static String M_002001					= "파일이 존재하지 않습니다.";
	public static String M_002002					= "파일 등록을 실패 하였습니다.";
	public static String M_002003					= "파일 삭제를 실패 하였습니다.";
	public static String M_002004					= "파일 삭제 권한이 없습니다.";
	public static String M_002005					= "회의 접속 토큰 생성을 실패 하였습니다.";
	
	//화이트보드 이미지
	public static String M_004001					= "이미지 등록을 실패 하였습니다.";
	public static String M_004002					= "이미지 수정을 실패 하였습니다.";
	public static String M_004003					= "이미지 삭제를 실패 하였습니다.";
	
	//토큰
	public static String M_005001					= "토큰 생성에 실패했습니다.";
	public static String M_005002					= "토큰정보 업데이트에 실패했습니다.";
	public static String M_005003					= "토큰 정보가 잘못되었습니다.";
	public static String M_005004					= "토큰이 유효하지 않습니다.";
	public static String M_005005					= "토큰 생성 정보가 잘못되었습니다.";
	public static String M_005006					= "토큰 만료시간이 경과하였습니다.";
	
	//기타
	public static String M_999999					= "시스템에 오류가 발생하였습니다.";
	
}
