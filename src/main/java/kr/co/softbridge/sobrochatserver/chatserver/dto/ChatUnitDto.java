package kr.co.softbridge.sobrochatserver.chatserver.dto;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "unit")
public class ChatUnitDto {
    private String domain;
    private String private_ip;
    private String public_ip;
    private int tcp_port;
    private int unit_fd;
    private int RoomCode;

}
