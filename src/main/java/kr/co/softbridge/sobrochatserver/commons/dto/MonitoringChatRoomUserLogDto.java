package kr.co.softbridge.sobrochatserver.commons.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringChatRoomUserLogDto {

	@ApiParam(value = "사용자ID", required = false, example = "1")
	private	String	userId;
	
	@ApiParam(value = "사용자명", required = false, example = "50")
	private	String	userNm;
	
	@ApiParam(value = "MAC주소", required = false, example = "ab:cd:ef:gh")
	private	String	macAddress;
	
	@ApiParam(value = "디바이스ID", required = false, example = "20210101000000")
	private	String	deviceId;
	
	@ApiParam(value = "채팅방 코드", required = false, example = "")
	private	String	chatRoomCode;
	
}
