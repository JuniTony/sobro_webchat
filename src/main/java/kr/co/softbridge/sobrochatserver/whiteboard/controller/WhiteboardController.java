package kr.co.softbridge.sobrochatserver.whiteboard.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardDeleteDto;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardDto;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardUpdateDto;
import kr.co.softbridge.sobrochatserver.whiteboard.dto.WhiteboardUploadDto;
import kr.co.softbridge.sobrochatserver.whiteboard.service.WhiteboardService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/whiteboard")
@RestController
public class WhiteboardController {
	
	private final WhiteboardService whiteboardService;
	
	/**
	 * <pre>
     * @Method Name : uploadWhiteboard
     * 1. 개요 		: 화이트보드 저장 (uploadWhiteboard)
     * 2. 처리내용 	: 화이트보드 저장 (uploadWhiteboard)
     * 3. 작성자	: user
     * 4. 작성일	: 2022. 5. 18.
     * </pre>
	 * 
	 * @param request
	 * @param JSON Map
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "화이트보드 저장")
    @PostMapping(value = "/uploadWhiteboard", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WhiteboardUploadDto> uploadWhiteboard(HttpServletRequest request, @RequestBody(required = true) HashMap<String, Object> paramMap) throws Exception {
		
		//Start Date Pattern 생성
		String pattern	= 	"yyyy-MM-dd HH:mm:ss";
    	String pattern2	=	"yyyyMMddHHmmssSSS";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
    	String startServerTime = simpleDateFormat.format(new Date());
    	String startServerTime2 = simpleDateFormat2.format(new Date());
    	paramMap.put("nowTime", startServerTime);
		
    	ResponseEntity<WhiteboardUploadDto> result = whiteboardService.uploadWhiteboard(request, paramMap);
    	
    	//End Date Pattern 생성
		String endServerTime = simpleDateFormat.format(new Date());
    	String endServerTime2 = simpleDateFormat2.format(new Date());
    	
    	return result;
	}
	
	/**
	 * <pre>
     * @Method Name : updateWhiteboard
     * 1. 개요 		: 화이트보드 수정 (updateWhiteboard)
     * 2. 처리내용 	: 화이트보드 수정 (updateWhiteboard)
     * 3. 작성자	: user
     * 4. 작성일	: 2022. 5. 18.
     * </pre>
	 * 
	 * @param request
	 * @param JSON Map
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "화이트보드 수정")
    @PostMapping(value = "/updateWhiteboard", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WhiteboardUpdateDto> updateWhiteboard(HttpServletRequest request, @RequestBody(required = true) HashMap<String, Object> paramMap) throws Exception {
		
		//Start Date Pattern 생성
		String pattern	= 	"yyyy-MM-dd HH:mm:ss";
    	String pattern2	=	"yyyyMMddHHmmssSSS";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
    	String startServerTime = simpleDateFormat.format(new Date());
    	String startServerTime2 = simpleDateFormat2.format(new Date());
    	paramMap.put("nowTime", startServerTime);
		
    	ResponseEntity<WhiteboardUpdateDto> result = whiteboardService.updateWhiteboard(request, paramMap);
    	
    	//End Date Pattern 생성
		String endServerTime = simpleDateFormat.format(new Date());
    	String endServerTime2 = simpleDateFormat2.format(new Date());
    	
    	return result;
    	
	}
	
	/**
	 * <pre>
     * @Method Name : deleteWhiteboard
     * 1. 개요 		: 화이트보드 삭제 (deleteWhiteboard)
     * 2. 처리내용 	: 화이트보드 삭제 (deleteWhiteboard)
     * 3. 작성자	: user
     * 4. 작성일	: 2022. 5. 18.
     * </pre>
	 * 
	 * @param request
	 * @param JSON Map
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "화이트보드 삭제")
    @PostMapping(value = "/deleteWhiteboard", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WhiteboardDeleteDto> deleteWhiteboard(HttpServletRequest request, @RequestBody(required = true) HashMap<String, Object> paramMap) throws Exception {
		
		//Start Date Pattern 생성
		String pattern	=	"yyyy-MM-dd HH:mm:ss";
    	String pattern2	=	"yyyyMMddHHmmssSSS";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
    	String startServerTime = simpleDateFormat.format(new Date());
    	String startServerTime2 = simpleDateFormat2.format(new Date());
    	paramMap.put("nowTime", startServerTime);
		
    	ResponseEntity<WhiteboardDeleteDto> result = whiteboardService.deleteWhiteboard(request, paramMap);
    	
    	//End Date Pattern 생성
		String endServerTime = simpleDateFormat.format(new Date());
    	String endServerTime2 = simpleDateFormat2.format(new Date());
    	
    	return result;
    	
	}
	
	/**
	 * <pre>
     * @Method Name : selectWhiteboard
     * 1. 개요 		: 화이트보드 조회 (selectWhiteboard)
     * 2. 처리내용 	: 화이트보드 조회 (selectWhiteboard)
     * 3. 작성자	: user
     * 4. 작성일	: 2022. 5. 18.
     * </pre>
	 * 
	 * @param request
	 * @param JSON Map
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "화이트보드 조회")
    @PostMapping(value = "/selectWhiteboard", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WhiteboardDto> selectWhiteboard(HttpServletRequest request, @RequestBody(required = true) HashMap<String, Object> paramMap) throws Exception {
		
		//Start Date Pattern 생성
		String pattern	= 	"yyyy-MM-dd HH:mm:ss";
    	String pattern2	=	"yyyyMMddHHmmssSSS";
    	
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
    	String startServerTime = simpleDateFormat.format(new Date());
    	String startServerTime2 = simpleDateFormat2.format(new Date());
    	paramMap.put("nowTime", startServerTime);
		
    	ResponseEntity<WhiteboardDto> result = whiteboardService.selectWhiteboard(request, paramMap);
    	
    	//End Date Pattern 생성
		String endServerTime = simpleDateFormat.format(new Date());
    	String endServerTime2 = simpleDateFormat2.format(new Date());
    	
    	return result;
    	
	}

}
