package kr.co.softbridge.sobrochatserver.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SvcTokenInfoResponseDto{

    private String svcToken;
    private String svcCode;
    private String resultCode;
    private String resultMsg;
    private String scvTokenIdx;
    private String userId;
    
}
