package kr.co.softbridge.sobrochatserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SobrochatserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(SobrochatserverApplication.class, args);
	}

}
