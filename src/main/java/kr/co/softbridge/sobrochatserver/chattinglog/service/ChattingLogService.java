package kr.co.softbridge.sobrochatserver.chattinglog.service;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.softbridge.sobrochatserver.chattinglog.dto.ChattingLogDto;
import kr.co.softbridge.sobrochatserver.chattinglog.dto.ChattingLogRequestDto;
import kr.co.softbridge.sobrochatserver.chattinglog.mapper.ChattingLogMapper;
import kr.co.softbridge.sobrochatserver.commons.constants.commonConstant;
import kr.co.softbridge.sobrochatserver.commons.dto.ChatRoomVerifyResponseDto;
import kr.co.softbridge.sobrochatserver.commons.exception.ChatServerException;
import kr.co.softbridge.sobrochatserver.commons.mapper.CommonTokenMapper;
import kr.co.softbridge.sobrochatserver.commons.service.CommonTokenService;
import kr.co.softbridge.sobrochatserver.commons.service.MonitoringLogService;
import kr.co.softbridge.sobrochatserver.commons.util.StringUtil;

@Service
public class ChattingLogService {
	
	private static final Logger logger = LogManager.getLogger(ChattingLogService.class);
	
    @Value("${jwt.secret}")
    private String secret; 
    
    @Value("${jwt.type.BRO}")
    private String BRO; 
    
    @Autowired
    private ChattingLogMapper chattingLogMapper;

	@Autowired
    private CommonTokenService commonTokenService;
	
	@Autowired
    private CommonTokenMapper commonTokenMapper;
	
	@Autowired
	private MonitoringLogService monitoringLogService;

	public ResponseEntity<ChattingLogDto> selectChattingLog(HttpServletRequest request, HashMap<String, Object> paramMap) throws Exception {
		ChattingLogDto meetingLogResult = null;
		try {
    		if(StringUtil.isEmpty((String) paramMap.get("chatRoomCode"))
			||StringUtil.isEmpty((String) paramMap.get("userId"))
			||StringUtil.isEmpty((String) paramMap.get("userNm"))
			||StringUtil.isEmpty((String) paramMap.get("chatRoomToken"))
    		) {
    			throw new ChatServerException("000001", commonConstant.M_000001);
    		}
			/* chatRoomToken 검증 */
			ChatRoomVerifyResponseDto chatRoomTokenInfo = commonTokenService.getChatRoomTokenInfo(request, paramMap);
			if("000000".equals(chatRoomTokenInfo.getResultCode())) {
	    		meetingLogResult = getChattingLog(paramMap);
	    		
	    		if(meetingLogResult == null) {
	    			return ResponseEntity.status(HttpStatus.OK)
	    	    			.body(ChattingLogDto
	    	    					.builder()
	    	    					.resultCode("000000")
	    	    					.resultMsg(commonConstant.M_000000)
	    	    					.build()); 
	    		}
			}else {
				throw new ChatServerException(chatRoomTokenInfo.getResultCode(), chatRoomTokenInfo.getResultMsg());
			}
		}catch (ChatServerException e) {
    		if(StringUtil.isNotEmpty(e.getErrorLogMsg())) {
				logger.info("[selectChattingLog] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getErrorLogMsg());
			}else {
				logger.info("[selectChattingLog] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getMessage());
			}
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(ChattingLogDto
	    					.builder()
	    					.resultCode(e.getErrorCode())
	    					.resultMsg(e.getMessage())
	    					.build());
		} catch(Exception e) {
    		logger.info("000003", commonConstant.M_000003, e.getMessage());
    		return ResponseEntity.status(HttpStatus.OK)
	    			.body(ChattingLogDto
	    					.builder()
	    					.resultCode("000003")
	    					.resultMsg(commonConstant.M_000003)
	    					.build());
		}
		
		return ResponseEntity.status(HttpStatus.OK)
    			.body(ChattingLogDto
    					.builder()
    					.chatRoomCode(meetingLogResult.getChatRoomCode())
    					.chattingLogSeq(meetingLogResult.getChattingLogSeq())
    					.contents(meetingLogResult.getContents())
    					.resultCode("000000")
    					.resultMsg(commonConstant.M_000000)
    					.build()); 
	}

	private ChattingLogDto getChattingLog(HashMap<String, Object> paramMap) {
		return chattingLogMapper.getChattingLog(paramMap);
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ChattingLogDto> saveChattingLog(HttpServletRequest request, ChattingLogRequestDto param) throws Exception {
		ChattingLogDto meetingLogResult = null;
		try {
			
			if(StringUtil.isEmpty((String) param.getChatRoomCode())
			||StringUtil.isEmpty((String) param.getUserId())
			||StringUtil.isEmpty((String) param.getUserNm())
			||StringUtil.isEmpty((String) param.getChatRoomToken())
			||StringUtil.isEmpty((String) param.getContents())
			||StringUtil.isEmpty((String) param.getSaveType())
    		) {
    			throw new ChatServerException("000001", commonConstant.M_000001);
    		}
			
			/* chatRoomToken 검증 */
    		HashMap<String, Object> paramMap = new HashMap<String, Object>();
    		paramMap.put("chatRoomCode", param.getChatRoomCode());
    		paramMap.put("chatRoomToken", param.getChatRoomToken());
    		paramMap.put("userId", param.getUserId());
    		paramMap.put("userNm", param.getUserNm());
    		paramMap.put("contents", param.getContents());
			ChatRoomVerifyResponseDto chatRoomTokenInfo = commonTokenService.getChatRoomTokenInfo(request, paramMap);
			if("000000".equals(chatRoomTokenInfo.getResultCode())) {
				param.setSvcCode(chatRoomTokenInfo.getSvcCode());
				int contentsCount = getMeetingLogCnt(paramMap);
				if(param.getSaveType().equals(commonConstant.SAVE_CRATE)) {
					if( contentsCount == 0 ) insertMettingLog(paramMap);
					else updateMettingLog(paramMap);
				} else if(param.getSaveType().equals(commonConstant.SAVE_UPDATE)) {
					if( contentsCount == 0 ) insertMettingLog(paramMap);
					else updateMettingLog(paramMap);
				} else {
					return ResponseEntity.status(HttpStatus.OK)
			    			.body(ChattingLogDto
			    					.builder()
			    					.resultCode("000002")
			    					.resultMsg(commonConstant.M_000002)
			    					.build());
				}
	    		meetingLogResult = getChattingLog(paramMap);
			}else {
				throw new ChatServerException(chatRoomTokenInfo.getResultCode(), chatRoomTokenInfo.getResultMsg());
			}
		}catch (ChatServerException e) {
    		if(StringUtil.isNotEmpty(e.getErrorLogMsg())) {
				logger.info("[saveChattingLog] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getErrorLogMsg());
			}else {
				logger.info("[saveChattingLog] ERROR_CODE=" + e.getErrorCode() + ", ERROR_MSG=" + e.getMessage());
			}
			return ResponseEntity.status(HttpStatus.OK)
	    			.body(ChattingLogDto
	    					.builder()
	    					.resultCode(e.getErrorCode())
	    					.resultMsg(e.getMessage())
	    					.build());
		} catch(Exception e) {
    		logger.info("000003", commonConstant.M_000003, e.getMessage());
    		return ResponseEntity.status(HttpStatus.OK)
	    			.body(ChattingLogDto
	    					.builder()
	    					.resultCode("000003")
	    					.resultMsg(commonConstant.M_000003)
	    					.build());
		}
		
		return ResponseEntity.status(HttpStatus.OK)
    			.body(ChattingLogDto
    					.builder()
    					.chatRoomCode(meetingLogResult.getChatRoomCode())
    					.chattingLogSeq(meetingLogResult.getChattingLogSeq())
    					.contents(meetingLogResult.getContents())
    					.resultCode("000000")
    					.resultMsg(commonConstant.M_000000)
    					.build());
	}

	private int getMeetingLogCnt(HashMap<String, Object> paramMap) {
		return chattingLogMapper.getChattingLogCnt(paramMap);
	}

	private void updateMettingLog(HashMap<String, Object> paramMap) {
		chattingLogMapper.updateChattingLog(paramMap);
	}

	private void insertMettingLog(HashMap<String, Object> paramMap) {
		chattingLogMapper.insertChattingLog(paramMap);
	}

}
