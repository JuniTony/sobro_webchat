package kr.co.softbridge.sobrochatserver.chattinglog.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChattingLogDto {
	
    private String chatRoomCode;
	
    private String chattingLogSeq;
	
    private String contents;
	
    private String resultCode;
	
    private String resultMsg;

}
