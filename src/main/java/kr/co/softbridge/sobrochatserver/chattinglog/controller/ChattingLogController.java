package kr.co.softbridge.sobrochatserver.chattinglog.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import kr.co.softbridge.sobrochatserver.chattinglog.dto.ChattingLogDto;
import kr.co.softbridge.sobrochatserver.chattinglog.dto.ChattingLogRequestDto;
import kr.co.softbridge.sobrochatserver.chattinglog.service.ChattingLogService;
import kr.co.softbridge.sobrochatserver.commons.service.MonitoringLogService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/chattinglog")
@RestController
public class ChattingLogController {

	@Value("${chatserver.aes256.key}")
    private String key;
	
	private final ChattingLogService chattingLogService;
    
    private final MonitoringLogService monitoringLogService;
    
    @ApiOperation(value = "채팅 조회", notes = "채팅 내용 조회")
    @PostMapping(value = "/viewChatting", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChattingLogDto> chattingLogView(HttpServletRequest request, @RequestBody(required = true) HashMap<String, Object> paramMap) throws Exception {
    	String pattern = "yyyy-MM-dd HH:mm:ss";
    	String pattern2 = "yyyyMMddHHmmssSSS";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
    	String startServerTime = simpleDateFormat.format(new Date());
    	String startServerTime2 = simpleDateFormat2.format(new Date());
    	
    	// 서비스 호출
    	ResponseEntity<ChattingLogDto> result = chattingLogService.selectChattingLog(request, paramMap);
    	
    	String endServerTime = simpleDateFormat.format(new Date());
    	String endServerTime2 = simpleDateFormat2.format(new Date());
    	
    	monitoringLogService.apiCallLog(startServerTime, endServerTime, "채팅 내용 조회", "R", result.getBody().getResultCode(), paramMap, result);
    	
    	return result;
    	
    }
    
    @ApiOperation(value = "채팅 저장", notes = "채팅 내용 저장")
    @PostMapping(value = "/saveChatting", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChattingLogDto> chattingLogSave(HttpServletRequest request,@RequestBody ChattingLogRequestDto param) throws Exception {
    	String pattern = "yyyy-MM-dd HH:mm:ss";
    	String pattern2 = "yyyyMMddHHmmssSSS";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
    	String startServerTime = simpleDateFormat.format(new Date());
    	String startServerTime2 = simpleDateFormat2.format(new Date());
    	
    	// 서비스 호출
    	ResponseEntity<ChattingLogDto> result = chattingLogService.saveChattingLog(request, param);

    	/* 통합로그를 위해 paramMap에 담음 */
    	HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("svcCode", param.getSvcCode());
		paramMap.put("chatRoomCode", param.getChatRoomCode());
    	
    	String endServerTime = simpleDateFormat.format(new Date());
    	String endServerTime2 = simpleDateFormat2.format(new Date());
    	monitoringLogService.apiCallLog(startServerTime, endServerTime, "채팅 내용 저장", "CU", result.getBody().getResultCode(), paramMap, result);
    	
    	return result;
    	
    }
}
