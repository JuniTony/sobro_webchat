package kr.co.softbridge.sobrochatserver.sample.dto;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class SampleinputDto {
	
	@ApiParam(value = "사용자 ID", required = false, example = "userId")
    private String userId;
    
	@ApiParam(value = "채팅방 번호", required = false, example = "1234")
    private int roomNo;
    
	@ApiParam(value = "인증Key", required = false, example = "sobro")
    private String encryptKey;
    
	@ApiParam(value = "암호화대상", required = false, example = "Sample입니다.")
    private String target;
}
