package kr.co.softbridge.sobrochatserver.commons.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import kr.co.softbridge.sobrochatserver.commons.dto.ChatServerServiceResDto;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("")
@RestController
public class ChatServerServiceController {

	@ApiOperation(value = "채팅서버서비스상태확인", notes = "현재 채팅서버 서버의 상태를 확인한다.")
	@GetMapping(value = "/chatserverservice", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ChatServerServiceResDto> chatServerServiceCheck() {
		
		ChatServerServiceResDto chatServerServiceResDto = new ChatServerServiceResDto();
		chatServerServiceResDto.setStatus(200);
		
		return ResponseEntity.ok().body(chatServerServiceResDto);
		
	}
}
