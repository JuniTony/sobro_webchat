const mediaHandlers = {
	success: function (type, on) {
		if (on) {
			if(type == "local") {
				sobro.attachMediaStream(document.querySelector("#dummy"), fStream[0]);
				sobro.muteAudio("ON", feeds[0]);
			} else if(type == "remote") {
				
			}
        }
	},
	error: function (error) {
		console.log("platform error", error);
	},
	onlocal: function (handler, stream) {
		feeds[handler.rfindex] = handler;
		if(!fStream[handler.rfindex]) {
			fStream[handler.rfindex] = stream;
		}
	},
	onremote: function (handler, stream) {
		handler.send({
			message: {
                "request": "configure",
                "send": false
            }
		});
	},
	streaminfo: function (rfid, bitrate, quality, speaking) {
		if(feeds[0]) {
			if(feeds[0].rfid == rfid) {
				$("#rfid").text(rfid);
				$("#bitrate").text(bitrate);
				$("#quality").text(quality);
				$("#speaking").text(speaking);
			}
		}
	},
	changesoundmode: function () {
		//feeds[0].unmuteVideo();
	},
	screencancel: function () {
		window.parent.postMessage({
		    'type': 'close'
		}, '*');
	},
	detached: function (rfid) {
		
	},
	disconnect: function () {
		mdDisconnect();
	}
}

function mdDisconnect() {
	if(fStream[0]) {
		if(fStream[0].getTracks().length > 0) {
			fStream[0].getTracks().forEach(function(track) {
				if(track) {
					track.stop();
				}
			});
		}
	}
	if(feeds[0]) {
		sobro.leave(feeds[0]);
	}
	$.ajax({
    	//url: apiUrl + apimediac +'/GetMediaInfo',
    	url: 'https://amcdb.sobro.co.kr:4000/GetMediaInfo',	//https://amcdb.sobro.co.kr:4000/GetMediaInfo
		data: {roomNo: roomNo, userId: userId},
        type: 'POST',
		dataType: 'json',
        success: function (data) {
			console.log("mdeiaInfo success", connectMediaCnt, data)
			if(data.DOMAIN && data.PORT) {
				connectMediaCnt = 0;
				sobro.setMediaUrl(data.DOMAIN, data.PORT);
				sobro.initMedia({apiUrl: apiUrl, mediaKey: data.KEY, type: "1", mode: "cam", roomNo: roomNo, publishercnt: 100, userId: userId, userNick: userId, quality: "02", screenType: "10", roomToken: "1234567890"}, mediaHandlers);
				
				//sobro.setMediaUrl("ulsp.sobro.co.kr", "9000");
				//sobro.setMediaUrl("ulsp.sobro.co.kr", "9001");
				//sobro.setMediaUrl("ulsp.sobro.co.kr", "9002");
				//sobro.setMediaUrl("ulsp.sobro.co.kr", "9003");
				//sobro.initMedia({apiUrl: apiUrl, mediaKey: "1234", type: "1", mode: "cam", roomNo: roomNo, publishercnt: 100, userId: userId, userNick: userNick, quality: "02", screenType: "10", roomToken: "1234567890"}, mediaHandlers);
				
			} else {
				console.log("mdeiaInfo error", connectMediaCnt)
				connectMediaCnt++;
				setTimeout(() => {
					mdDisconnect();
				}, 5000);
			}
        },
        error: function () {
			console.log("mdeiaInfo error", connectMediaCnt)
			connectMediaCnt++;
			setTimeout(() => {
				mdDisconnect();
			}, 5000);
        }
	});
}