package kr.co.softbridge.sobrochatserver.chattinglog.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChattingLogRequestDto {

	private String saveType = "C";
    
    private String chatRoomCode;

    private String contents;

    private String chatRoomToken;
    
    private String svcCode;
    
    private String userId;
    
    private String userNm;

}
