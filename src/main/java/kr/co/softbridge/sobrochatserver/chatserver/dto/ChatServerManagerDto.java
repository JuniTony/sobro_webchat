package kr.co.softbridge.sobrochatserver.chatserver.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/***
 * 채팅 서버 관리 Dto
 */
@Getter
@Setter
@Builder
public class ChatServerManagerDto {

    private String roomCode;

    private LocalDateTime startDt;

    private LocalDateTime endDt;

    private String targetNm;

    private String regId;

    private LocalDateTime regDt;

    private String udtId;

    private LocalDateTime udtDt;

    private String delYn;
}
