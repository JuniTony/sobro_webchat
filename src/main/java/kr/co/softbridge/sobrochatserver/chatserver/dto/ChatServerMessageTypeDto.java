package kr.co.softbridge.sobrochatserver.chatserver.dto;

/**
 * 채팅 서버 Message Type DTO
 */
public enum ChatServerMessageTypeDto {
    CHAT,
    JOIN,
    LEAVE
}
