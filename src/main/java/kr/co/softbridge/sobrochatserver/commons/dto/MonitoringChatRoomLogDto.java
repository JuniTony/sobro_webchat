package kr.co.softbridge.sobrochatserver.commons.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringChatRoomLogDto {
	
	@ApiParam(value = "채팅방 코드", required = true, example = "1")
	private	String	chatRoomCode;
	
	@ApiParam(value = "채팅방 시작일시", required = false, example = "20210101000000")
	private	String	startDt;
	
	@ApiParam(value = "채팅방 종료일시", required = false, example = "20210101235959")
	private	String	endDt;
	
	@ApiParam(value = "채팅방 진행시간", required = false, example = "2:00:00")
	private	String	progDt;
	
	@ApiParam(value = "생성타입", required = false, example = "C")
	private	String	type;
	
	@ApiParam(value = "생성자ID", required = false, example = "streamer1")
	private	String	hostId;
	
}
